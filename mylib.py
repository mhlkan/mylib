#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import webbrowser
from bs4 import BeautifulSoup
import requests
import pyautogui as pag
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from PIL import ImageGrab
from pygame import mixer
from random import choice
import sqlite3 as sql
from googlesearch import search
from googletrans import Translator
import pyttsx3
import speech_recognition as sr
from time import sleep as wait
import os
import sys
import difflib
import re
class Chtl:
    def __init__(self, i: str, o: str, align: int = 0, oalign: int = 0, inp: int = 0):  # '> ',' <'
        self.clog = ()
        self.i, self.o = i, o
        self.align = align
        self.oalign = oalign
        self.inp = inp

    def clear(self):
        os.system('cls')

    def printf(self, x: str):
        print(x)
        self.clog = ('~:' + x,) + self.clog

    def gin(self, x: str = '', withi=input):
        y = str(withi(self.inp * ' ' + x))
        self.clog = ('~;' + y,) + self.clog
        return y

    def getl(self, cl: bool = True):
        if cl:
            self.clear()
        for s in self.log[::-1]:
            if s[:2] in '~;':
                align = self.align - len(s)
                print(align * ' ' + s[2:] + self.i)
            elif s[:2] in '~:':
                print(self.oalign * ' ' + self.o + s[2:])

    def cllog(self):
        self.clog = ()

    @property
    def log(self):
        return self.clog


class Constant:
    class RebindingError(TypeError):
        pass

    def __setattr__(self, name, value):
        if name in self.__dict__:
            raise self.RebindingError("Cant't rebind Constant {}".format(name))
        self.__dict__[name] = value


class Dyba:
    def __init__(self, database_name: str):
        global con, c
        con = sql.connect(database_name)
        c = con.cursor()

    def ex(self, cmd: str):
        c.execute(cmd)

    def kill(self):
        con.close()

    class Table:
        def __init__(self, tn, vals: tuple):
            c.execute("CREATE TABLE IF NOT EXISTS {} ({})".format(tn, ','.join(vals)))
            self.tn = tn

        def ex(self, cmd: str):
            # cmd = INSERT: ('a','b')
            cmd = cmd.split(': ')
            # cmd = ['INSERT','('a','b')']
            if cmd[0].upper() == 'INSERT':
                print('INSERT INTO %s VALUES %s' % (self.tn, cmd[1]))
                c.execute('INSERT INTO %s VALUES %s' % (self.tn, cmd[1]))
                con.commit()
            elif cmd[0].upper() == 'GET':
                c.execute('SELECT %s FROM %s' % (cmd[1], self.tn))
                return c.fetchall()
            elif cmd[0].upper() == 'DELETE':
                cmd2 = cmd[1].split(' at ')
                c.execute("DELETE FROM %s WHERE %s = %s" % (self.tn, cmd2[0], cmd2[1]))
                con.commit()
            elif cmd[0].upper() == 'UPDATE':
                cmd2 = cmd[1].split(' at ')
                cmd3 = cmd2[1].split(' to ')
                cmd1 = cmd2[:1] + cmd3
                c.execute("UPDATE %s set %s = %s where %s = %s " % (
                    self.tn, cmd1[1], cmd1[2], cmd1[1], cmd1[0]))
                con.commit()
            else:
                quit("UNKNOWN COMMAND")

        def insert(self, *values):
            c.execute('INSERT INTO %s VALUES (%s)' % (self.tn, ','.join(values)))
            con.commit()

        def get(self, x: str):
            c.execute('SELECT %s FROM %s' % (x, self.tn))
            return c.fetchall()

        def delete(self, i, f):
            c.execute("DELETE FROM %s WHERE %s = %s" % (self.tn, f, i))
            con.commit()

        def update(self, o, n, f):
            c.execute("UPDATE %s set %s = %s where %s = %s" % (self.tn, f, n, f, o))
            con.commit()
class Funche:
    def __init__(self):
        self.funcs = {}

    def Sfunc(self, func):
        self.funcs[func.__name__] = func

    def runrf(self):
        luckyf = choice(list(self.funcs.keys()))
        x = self.funcs[luckyf]()
        return x

    def runall(self):
        returns = []
        for x in self.funcs:
            y = self.funcs[x]()
            returns.append(y)
        return returns


class Todo:
    def __init__(self):
        self.todos = []

    def atodo(self, thing):
        self.todos.append(str(thing))

    def rtodo(self, ft: int = 0):
        if not ft:
            return choice(self.todos)
        return choice(self.todos[:ft])

    def ctodo(self, thing):
        self.todos.remove(thing)

    @property
    def stodos(self):
        return self.todos


def uniq(l) -> list:
    l1 = []
    for v in l:
        if not v in l1:
            l1.append(v)
    return l1


def similar(w1, w2) -> float:
    w1 = w1 + ' ' * (len(w2) - len(w1))
    w2 = w2 + ' ' * (len(w1) - len(w2))
    return sum(1 if i == j else 0 for i, j in zip(w1, w2)) / float(len(w1))


def seqr(x1, x2) -> float:
    x = difflib.SequenceMatcher(None, x1, x2)
    return x.ratio()


def div(nums: list, *nums2):
    if nums:
        num = nums[0] / nums[1]
        for n in nums[2:]:
            num /= n
        return num
    else:
        num = nums2[0] / nums2[1]
        for n in nums2[2:]:
            num /= n
        return num


def sub(nums: list, *nums2):
    if nums:
        num = nums[0] - nums[1]
        for n in nums[2:]:
            num -= n
        return num
    else:
        num = nums2[0] - nums2[1]
        for n in nums2[2:]:
            num -= n
        return num


def mul(nums: list, *nums2):
    if nums:
        num = nums[0] * nums[1]
        for n in nums[2:]:
            num *= n
        return num
    num = nums2[0] * nums2[1]
    for n in nums2[2:]:
        num *= n
    return num


global eng
eng = pyttsx3.init()


def tell(text):
    eng.say(text)
    eng.runAndWait()
    return text


def phrase(text=''):
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print(text, end='')
        audio = r.listen(source)
    try:
        return r.recognize_google(audio)
    except:
        return ''


def cpunc(text: str):
    return re.sub(r'[^\w\s]', '', text)


def news(nums_of_news:int=9):
    url = "https://news.google.com/news/?ned=us&gl=US&hl=en"
    resp = requests.get(url)
    cont = resp.content
    soup = BeautifulSoup(cont, "html.parser")
    enews = {}
    for new in soup.find_all("a", {"class": "nuEeue hzdq5d ME7ew"})[:nums_of_news]:
        enews[new.text] = new.get('href')
    return enews
def ss() -> None:
    Image = ImageGrab.grab()
    Image.save("screenshots/screenshot.png")


def mailt(e, p, t, m):
    mail = MIMEMultipart()
    msg = MIMEText(m, "plain")
    mail["From"] = e
    mail["To"] = t
    mail["Subject"] = "A Mail From "+e
    mail.attach(msg)
    meil = smtplib.SMTP("smtp.gmail.com", 587)
    meil.ehlo()
    meil.starttls()
    meil.login(e, p)
    meil.sendmail(mail["From"], mail["To"], mail.as_string())


def write(this):
    if this[:5] in "enter":
        pag.keyDown('enter')
        pag.keyUp('enter')
        return True
    elif 'space' == this:
        pag.typewrite(' ')
    elif this[:9] in "stop type":
        return True
    else:
        pag.typewrite(this)
        return False
# search


def g(this):
    engine = "https://www.google.com/?#q="
    webbrowser.open(engine + this, new=2)


def gsot(this, lim: int=0):
    urls = []
    for x in search(this):
        urls.append(x)
        lim -= 1
        if not lim: break
    return urls
def wa(this):
    engine = "https://www.wolframalpha.com/input/?i="
    webbrowser.open(engine + this, new=2)


def y(this):
    engine = "https://www.youtube.com/results?search_query="
    webbrowser.open(engine + this, new=2)


def gb(this):
    engine = "https://www.google.com/search?tbm=bks&q="
    webbrowser.open(engine + this, new=2)


def gm(this):
    engine = "https://www.google.com.tr/maps/place/"
    webbrowser.open(engine + this, new=2)


def oget(this):
    webbrowser.open(this, new=2)

def transl(t: str, l: str):
    c = Translator()
    result = c.translate(t,dest=l)
    return result.text
class Radio:
    def __init__(self):
        mixer.init()
    def playf(self,path):
        mixer.music.load(path)
        mixer.music.play()
        while mixer.music.get_busy():
            wait(0.0001)
    def play(self,path):
        mixer.music.load(path)
        mixer.music.play()
    def pause(self):
        mixer.music.pause()

    def unpause(self):
        mixer.music.unpause()

    def stop(self):
        mixer.music.stop()

def clear():
    os.system('cls')

def rand(l):
    return choice(l)

def anim(t:str,i:float):
    try:
        for c in t:
            wait(i)
            sys.stdout.write(c)
            sys.stdout.flush()
        else: print('')
        return 0
    except:
        return 1
